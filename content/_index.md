---
title: Documentation
---

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD033 -->

This page is a home page for project documentation.

Project consist of 2 parts:
- web 
- backend

Project can configure other projects. It also have open API. 

## Features

- Clean and simple design
- Light and mobile-friendly
- Easy customization
- Zero initial configuration
- Handy shortcodes
